#import "CDVInflightEntertainment.h"
#import <Cordova/CDVPlugin.h>
#import <PACMetadata/PACMetadata.h>
#import <PACPassengerData/PACPassengerData.h>

@interface CDVInflightEntertainment ()

@property (nonatomic, retain) PACMetadataV1 *metadata;
@property (nonatomic, retain) id<PACMetadataV1Request> metadataRequest;
@property (nonatomic, retain)  PACPassengerDataV1* passengerDataService;

@end

@implementation CDVInflightEntertainment

- (void)initialize:(CDVInvokedUrlCommand *)command
{
    [InFlight setApplicationId:@"ccae8e1fbe8e3ffa773bfc527ae1b2b19b6a697be563f3b132358aa1e377~7pc1399f0e1b57f57c4653de74e3433dfbe"];

    [PACMetadataV1 initServiceWithCompletionBlock:^(id serviceObject, NSError *error) {
        if (error != nil) {
            NSLog(@"Failed to initialize service MetadataV1. Reason: %@",error);
            return;
        }
        self.metadata = serviceObject;
        
        [PACPassengerDataV1 initServiceWithCompletionBlock:^(id serviceObject, NSError* error) {
             if (error != nil) {
                 NSLog (@"Failed to initialize the service PACPassengerDataV1. Reason: %1$@",error);
             }
             self.passengerDataService = serviceObject;
         }];

        CDVPluginResult* pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK];
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];

}

- (void)retrieveMovies:(CDVInvokedUrlCommand *)command
{
    if (!self.metadataRequest) {
        self.metadataRequest = [self.metadata requestMediaListWithCategoryID:@"108" completionHandler:^(id object, NSError *error) {
            CDVPluginResult* pluginResult = nil;
            if (!error) {
                NSError* error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:(NSArray *)object options:NSJSONWritingPrettyPrinted error:&error];
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];

                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            } else {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
            }
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    } else {
        [self.metadataRequest start];
    }
}

- (void)retrieveSongs:(CDVInvokedUrlCommand *)command
{
    if (!self.metadataRequest) {
        self.metadataRequest = [self.metadata requestMediaListWithCategoryID:@"300" completionHandler:^(id object, NSError *error) {
            CDVPluginResult* pluginResult = nil;
            if (!error) {
                NSError* error;
                NSData *jsonData = [NSJSONSerialization dataWithJSONObject:(NSArray *)object options:NSJSONWritingPrettyPrinted error:&error];
                NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
                
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
            } else {
                pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
            }
            [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
        }];
    } else {
        [self.metadataRequest start];
    }
}

- (void)retrievePassenger:(CDVInvokedUrlCommand *)command
{
    NSString *seatNumber = [command.arguments objectAtIndex:0];
    [self.passengerDataService retrieveCurrentPassengerDetailsWithSeatNumber:seatNumber queue:[NSOperationQueue mainQueue] completionHandler:^(NSDictionary *passengerData, NSError *error){
        CDVPluginResult* pluginResult = nil;
        if (!error) {
            NSError* error;
            NSData *jsonData = [NSJSONSerialization dataWithJSONObject:passengerData options:NSJSONWritingPrettyPrinted error:&error];
            NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
            
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:jsonString];
        } else {
            pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR];
        }
        [self.commandDelegate sendPluginResult:pluginResult callbackId:command.callbackId];
    }];
}

@end
