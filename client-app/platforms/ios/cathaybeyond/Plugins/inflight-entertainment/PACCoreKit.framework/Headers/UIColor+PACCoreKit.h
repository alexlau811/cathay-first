/** @module PACCoreKit.framework *///
//  UIColor+PACCoreKit.h
//  PACCoreKit
//
//  Created by Gary L. Wade (MLS Technologies) on 12/06/2015.
//  Copyright © 2015 Panasonic Avionics Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (PACCoreKit)

+(UIColor* )panasonicBlueColor;

+(UIColor* )disabledLightGrayColor;

@end
