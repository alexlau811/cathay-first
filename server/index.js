var FB = require('fb')
var axios = require('axios')
var express = require('express')
var app = express();
var server = require('http').Server(app)
var io = require('socket.io')(server)

app.use(express.static('public'));

app.get('/me', function(req, res) {
  FB.setAccessToken(req.query.token);
  console.log('me');
  FB.api('/me', 'GET', {"fields":"name,languages"}, function (response) {
    if(!response || response.error) {
     console.log(!response ? 'error occurred' : response.error);
     return;
    }

    res.json(response);
  });
});

app.get('/likes', function(req, res) {
  FB.setAccessToken(req.query.token);
  
  FB.api('/me/likes', 'GET', {"limit":"9999"}, function (response) {
    if(!response || response.error) {
     console.log(!response ? 'error occurred' : response.error);
     return;
    }

    mergeData(response, d => d.name, result => res.json(result));
  });
});

app.get('/movies', function(req, res) {
  FB.setAccessToken(req.query.token);
  
  FB.api('/me/movies', 'GET', {"limit":"9999"}, function (response) {
    if(!response || response.error) {
     console.log(!response ? 'error occurred' : response.error);
     return;
    }

    mergeData(response, d => d.name, result => res.json(result));
  });
});


app.get('/books', function(req, res) {
  FB.setAccessToken(req.query.token);
  
  FB.api('/me/books', 'GET', {"limit":"9999"}, function (response) {
    if(!response || response.error) {
     console.log(!response ? 'error occurred' : response.error);
     return;
    }

    mergeData(response, d => d.name, result => res.json(result));
  });
});

function mergeData(response, map, next) {
  var result = [];
  if (response.paging.next != null) {
    fetchNext(response.paging.next, function (response) {
      result = result.concat(response.data.data.map(map));

      if (response.data.paging != null && response.data.paging.next != null) {
        return response.data.paging.next;
      } else {
        next(result);
      }
    });
  } else {
    result = result.concat(response.data.map(map));
    next(result);
  }
}

function fetchNext(next, func) {
  return axios.get(next).then(function(res) {
    var next = func(res);
    if (next != null) {
      return fetchNext(next, func);
    }
  }).catch(console.log);
}

app.get('/add100', function(req, res) {
  io.emit('add100'); 
});

app.get('/add200', function(req, res) {
  io.emit('add200'); 
});

app.get('/add500', function(req, res) {
  io.emit('add500'); 
});

io.on('connection', function (socket) {
});

server.listen(3000)