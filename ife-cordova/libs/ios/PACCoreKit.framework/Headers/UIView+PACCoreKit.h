/** @module PACCoreKit.framework *///
//  UIView+PACCoreKit.h
//  PACCoreKit
//
//  Created by Gary L. Wade (MLS Technologies) on 02/12/2016.
//  Copyright © 2016 Panasonic Avionics Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (PACCoreKit)

@property (readwrite,nonatomic,assign)  UIColor*    borderColor;
@property (readwrite,nonatomic,assign)  CGFloat     borderCornerRadius;
@property (readwrite,nonatomic,assign)  CGFloat     borderWidth;

-(void)setBorderColorIsTinted:(id )nilValue;

-(UIView* )superviewOfClass:(Class )class;

-(void)enumerateSubviewsWithKindOfClass:(Class )class
    block:(void(^)(UIView* aViewMatchingClass,BOOL* stop) )block;

@end
