/** @module PACCoreKit.framework *///
//  UIResponder+PACCoreKit.h
//  PACCoreKit
//
//  Created by Gary L. Wade (MLS Technologies) on 03/25/2016.
//  Copyright © 2016 Panasonic Avionics Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIResponder (PACCoreKit)

+(id )currentFirstResponder;

@end
