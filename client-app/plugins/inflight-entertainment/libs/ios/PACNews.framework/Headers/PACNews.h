/** @module PACNews.framework *///
//  Advertising.h
//  Advertising
//
//  Created by Rawad Hilal on 2/16/16.
//  Copyright © 2016 Panasonic Avionics Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

// News V1
#import <PACNews/PACNewsV1.h>
#import <PACNews/PACNewsV1Article.h>
#import <PACNews/PACNewsV1ArticleImage.h>
#import <PACNews/PACNewsV1ArticleSummary.h>
#import <PACNews/PACNewsV1Category.h>
#import <PACNews/PACNewsV1Provider.h>
