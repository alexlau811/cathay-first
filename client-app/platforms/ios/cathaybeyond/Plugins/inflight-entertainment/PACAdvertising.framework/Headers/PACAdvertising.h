/** @module PACAdvertising.framework *///
//  Advertising.h
//  Advertising
//
//  Created by Rawad Hilal on 2/16/16.
//  Copyright © 2016 Panasonic Avionics Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

// One media
#import <PACAdvertising/PACAdvertisingV1.h>
#import <PACAdvertising/PACAdvertisingItem.h>
