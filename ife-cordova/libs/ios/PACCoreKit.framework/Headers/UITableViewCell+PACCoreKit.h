/** @module PACCoreKit.framework *///
//  UITableViewCell+PACCoreKit.h
//  PACCoreKit
//
//  Created by Gary L. Wade (MLS Technologies) on 12/15/2015.
//  Copyright © 2015 Panasonic Avionics Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (PACCoreKit)

-(CGFloat )variableHeightForSubtitleStyleInTableView:(UITableView* )tableView;

@end
