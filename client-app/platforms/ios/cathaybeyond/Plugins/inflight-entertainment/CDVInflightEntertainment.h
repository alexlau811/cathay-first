#import <Cordova/CDVPlugin.h>

@interface CDVInflightEntertainment : CDVPlugin

- (void)initialize:(CDVInvokedUrlCommand *)command;
- (void)retrieveMovies:(CDVInvokedUrlCommand *)command;
- (void)retrieveSongs:(CDVInvokedUrlCommand *)command;
- (void)retrievePassenger:(CDVInvokedUrlCommand *)command;

@end