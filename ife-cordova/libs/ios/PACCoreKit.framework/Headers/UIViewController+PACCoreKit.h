/** @module PACCoreKit.framework *///
//  UIViewController+PACCoreKit.h
//  CrewMessageSDKTest
//
//  Created by Gary L. Wade (MLS Technologies) on 12/05/2015.
//  Copyright © 2015 Panasonic Avionics Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIViewController (PACCoreKit)

-(void)presentViewControllerNamed:(NSString* )nameOfViewController
    fromStoryboardWithName:(NSString* )nameOfStoryboard
    bundle:(nullable NSBundle* )storyboardBundleOrNil
    animated:(BOOL )animated
    completion:(void(^ __nullable)(void) )completion;

+ (UIViewController * __nullable)topViewController;

-(void)replaceViewWithNewSuperviewAndSiblingView:(UIView* )newSiblingView
    positionSiblingViewAboveOriginalView:(BOOL )positionSiblingAboveUs;

-(void)showChildViewController:(UIViewController* )childViewController
    coveringSubview:(UIView* )subview
    animated:(BOOL )animated;

-(void)hideChildViewController:(UIViewController* )childViewController
    coveringSubview:(UIView* )subview
    animated:(BOOL )animated;

-(void)attachWithinSuperviewTopConstraint:(NSNumber* )topConstraintValue
    leadingConstraint:(NSNumber* )leadingConstraintValue
    bottomConstraint:(NSNumber* )bottomConstraintValue
    trailingConstraint:(NSNumber* )trailingConstraintValue;

-(void)addSubviewController:(UIViewController* )childViewController
    intoView:(UIView* )subview
    topConstraint:(NSNumber* )topConstraintValue
    leadingConstraint:(NSNumber* )leadingConstraintValue
    bottomConstraint:(NSNumber* )bottomConstraintValue
    trailingConstraint:(NSNumber* )trailingConstraintValue
    layoutIfNeeded:(BOOL )layoutIfNeeded;

@end

NS_ASSUME_NONNULL_END
