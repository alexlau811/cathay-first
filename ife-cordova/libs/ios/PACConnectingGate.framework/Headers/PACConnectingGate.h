/** @module PACConnectingGate.framework *///
//  PACConnectingGate.h
//  PACConnectingGate
//
//  Created by Gary L. Wade on 05/18/2016.
//  Copyright © 2016 Panasonic Avionics Corporation. All rights reserved.
//

#ifndef PACConnectingGate_h
#define PACConnectingGate_h

#import <PACConnectingGate/PACConnectingGateV1.h>

#endif /* PACConnectingGate_h */
