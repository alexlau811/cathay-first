/** @module PACPassengerData.framework *///
//  PACPassengerDataV1.h
//  PACPassengerData
//
//  Created by Sedinam Gadzekpo on 9/8/17.
//  Copyright ¬© 2017 Panasonic Avionics Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACCoreKit/PACCoreKit.h>

@interface PACPassengerDataV1 : InFlightService

FOUNDATION_EXPORT NSString * _Nonnull const PACPassengerDataErrorDomain;


/**
 PACPassengerDataV1 retrieves passenger related information for passengers that log in to the seatback monitor.
 
 Passengers can retrieve their profile, which can include name, demographics, preferences, itinerary and playlists.  In addition, passengers can retrieve basic information, such as name, demographics and destination airport, for all passengers on the flight.
 */

/**
 * Request error codes.
 *
 * @owner PACPassengerDataV1
 */
typedef enum {
    /** The request has failed due to an unknown error. */
    PACPassengerDataErrorUnknown = 1000,
    /** The request has missing or incorrect parameters. */
    PACPassengerDataErrorBadRequest = 1001,
    /** The request has failed due to the service being unavailable. */
    PACPassengerDataErrorServiceNotFound = 1002,
    /** The request has failed due to an unexpected internal error. */
    PACPassengerDataErrorInternalError = 1003,
    /** The request has failed due to an unexpected response from the server.
     */
    PACPassengerDataErrorBadResponse = 1004,
    /** The request has failed due to a network connection error. */
    PACPassengerDataErrorConnectionError = 1005,
    /** The request has failed due to the passenger data not being found.
     */
    PACPassengerDataErrorDataNotFound = 1006,
}PACPassengerDataErrorCode;


/** Completion block called by request in this service */
typedef void (^PACPassengerDataCompletionHandler)(NSDictionary* _Nullable passengerData, NSError * _Nullable error);

/** Retrieves all details from the logged in passengerís profile.  This could include name, demographics, preferences, itinerary and playlists.
 */
-(void)retrieveCurrentPassengerDetailsWithSeatNumber:(NSString * _Nonnull) seatNumber
                                               queue:(NSOperationQueue * _Nonnull)queue
                                   completionHandler:(PACPassengerDataCompletionHandler _Nullable )completion;

/** Retrieves basic information, such as seat number, first name and destination, for all passengers on the flight.
 */
-(void)retrievePassengersInformationWithQueue:(NSOperationQueue * _Nonnull)queue completionHandler:(PACPassengerDataCompletionHandler _Nullable )completion;

@end

