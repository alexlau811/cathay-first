/** @module PACMetadata.framework *///
//  Metadata.h
//  Metadata
//
//  Created by Rawad Hilal on 1/15/16.
//  Copyright © 2016 Panasonic Avionics Corporation. All rights reserved.
//

#ifndef Metadata_h
#define Metadata_h

#import <PACMetadata/PACMetadataV1.h>

#endif /* Metadata_h */
