/** @module PACCatalogBrowsing.framework *///
//  CatalogBrowsing.h
//  CatalogBrowsing
//
//  Created by Rawad Hilal on 1/14/16.
//  Copyright © 2016 Panasonic Avionics Corporation. All rights reserved.
//

#ifndef Catalogdata_h
#define Catalogdata_h

#import <PACCatalogBrowsing/PACCatalogdataV1.h>

#endif /* Catalogdata_h */
