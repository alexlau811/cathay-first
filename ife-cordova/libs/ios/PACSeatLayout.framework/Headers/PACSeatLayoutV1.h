/** @module PACSeatLayout.framework *///
//  PACSeatLayoutV1.h
//  PACSeatLayout
//
//  Created by Sedinam Gadzekpo on 9/8/17.
//  Copyright © 2017 Panasonic Avionics Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PACCoreKit/PACCoreKit.h>

@interface PACSeatLayoutV1 : InFlightService

FOUNDATION_EXPORT NSString * _Nonnull const PACSeatLayoutErrorDomain;

/**
 * Request error codes.
 *
 * @owner PACSeatLayoutV1
 */
typedef enum {
    /** The request has failed due to an unknown error. */
    PACSeatLayoutErrorUnknown = 1000,
    /** The request has missing or incorrect parameters. */
    PACSeatLayoutErrorBadRequest = 1001,
    /** The request has failed due to the unavailability of the service. */
    PACSeatLayoutErrorServiceNotFound = 1002,
    /** The request has failed due to an unexpected internal error. */
    PACSeatLayoutErrorInternalError = 1003,
    /** The request has failed due to an unexpected response from the server.
     */
    PACSeatLayoutErrorBadResponse = 1004,
    /** The request has failed due to a network connection error. */
    PACSeatLayoutErrorConnectionError = 1005,
    /** The request has failed due to the seat layout not being found.
     */
    PACSeatLayoutErrorLayoutNotFound = 1006
}PACSeatLayoutErrorCode;



/** Completion block called by request in this service */
typedef void (^PACSeatLayoutCompletionHandler)(NSDictionary* _Nullable seatLayout, NSError * _Nullable error);
/** Retrieves the details of the aircraft interior, such as the location of passenger and flight attendant seats, with x,y coordinates.
 
 This information could be used by developers to create a 3D model of the aircraft interior for Augmented Reality (AR) games.
 
 Sample response
         {
            "aircraft_info": {
            "model": "380",
            "series": "800"
         },
            "dimensions": {
                "deck": [
            {
                "index": "0",
                "name": "Upper",
                "left": "120",
                "length": "9000",
                "scale_Y": "110",
                "min_y_coord": "405",
                "max_y_coord": "2400",
                "max_x_coord": "120"
            }
         },
            "classes": [
            {
                "name": "First",
                "key": "FRS", // class abbreviation
                "cabin": "0", // location of cabin
                "seats": "6" // number of seats in cabin
         },
            "seats": {
                "row": [
            {
                    "seat": [
                    {
                        "letter": "A",
                        "deck": "0",
                        "y_coord": "750",
                        "x_coord": "-75",
                        "block": "1", //seat row between aisles
                        "cabin": "0"
                    },
                    {
                        "letter": "F",
                        "deck": "0",
                        "y_coord": "750",
                        "x_coord": "75",
                        "block": "2",
                        "cabin": "0"
                    }
                ],
                    "row_number": "1",
                    "exit_row": false
                },
         }
 
 */
-(void)retrieveSeatLayoutWithQueue:(NSOperationQueue * _Nonnull)queue
                 completionHandler:(PACSeatLayoutCompletionHandler _Nonnull )completion;


@end



