/** @module PACCoreKit.framework *///
//  Version.h
//  InFlight iOS SDK
//
//  Copyright (c) 2015 Panasonic Avionics Corporation. All rights reserved.
//

#ifndef InFlight_iOS_SDK_Version_h
#define InFlight_iOS_SDK_Version_h

#define LIBRARY_VERSION @"03.10.00.08b"

#endif
