/** @module PACCoreKit.framework *///
//  PACUITableViewCellStyleSubtitleCell.h
//  CrewMessageSDKTest
//
//  Created by Gary L. Wade (MLS Technologies) on 03/04/2016.
//  Copyright © 2016 Panasonic Avionics Corporation. All rights reserved.
//

#import <UIKit/UIKit.h>

FOUNDATION_EXPORT NSString* const   PACUITableViewCellStyleSubtitleCellNIB;
FOUNDATION_EXPORT NSString* const   PACUITableViewCellStyleSubtitleCellIdentifier;
FOUNDATION_EXPORT const NSInteger   PACUITableViewCellStyleSubtitleCell_TitleTag;
FOUNDATION_EXPORT const NSInteger   PACUITableViewCellStyleSubtitleCell_SubtitleTag;
