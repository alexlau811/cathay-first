/** @module PACWeather.framework *///
//  PACWeatherV1Weather.h
//  Weather
//
//  Copyright © 2016 Panasonic Avionics Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

/**
 * This class is a model structure for weather.
 */
@interface PACWeatherV1CurrentWeatherItem : NSObject

/**
 * Human readable weather condition, such as sunny or cloudy.
 *
 */
@property (nonatomic, copy) NSString * _Nonnull condition;

/**
 * The weather condition code. For example, the code for sunny is 1001 and for cloudy is 3009.
 *
 * Please refer to PACWeatherV1WeatherConditionCode for weather conditions.
 */
@property (nonatomic, copy) NSNumber * _Nonnull conditionCode;

/**
 * The current temperature.
 *
 * Temperatures are in degrees Celsius.
 */
@property (nonatomic, copy) NSNumber * _Nonnull currentTemperature;

/**
 * Unique 4-letter ICAO city code.
 */
@property (nonatomic, copy) NSString * _Nonnull ICAOCode;

/**
 * The WeatherIcon class provides only distinct weather condition names which can be used to distinguish the icons in your icon set.
 *
 * The WeatherIcon class provides only distinct weather condition names which can be used to distinguish the icons in your icon set. The image files for each icon grouping are not provided by the API and must be included in your application. The application developer must provide their own image assets.
 * Please refer to PACWeatherV1WeatherIcons for a selection of icons.
 */
@property (nonatomic, copy) NSString * _Nonnull iconName;

/**
 * The city name.
 */
@property (nonatomic, copy) NSString * _Nonnull cityName;

/**
 * The region name.
 */
@property (nonatomic, copy) NSString * _Nonnull regionName;

/**
 * The region id.
 */
@property (nonatomic, copy) NSString * _Nonnull regionId;

/**
 * The date and time that the weather data was retrieved on.
 */
@property (nonatomic, copy) NSDate * _Nonnull timestamp;

@end
