/** @module PACConnectingGate.framework *///
//  PACConnectingGateV1.h
//  InFlight iOS SDK
//  Copyright (c) 2016 Panasonic Avionics Corporation. All rights reserved.
//

#import <PACCoreKit/PACCoreKit.h>

/**
The PACBasicAirportInfo class is an object that provides the basic airport information, such the IATA code and airport by city name.
 */
@interface PACBasicAirportInfo : NSObject

/** The 3-letter IATA code for the airport */
@property (readonly,nonatomic,copy,nonnull)     NSString*   iataCode;

/** The city name for the airport */
@property (readonly,nonatomic,copy,nullable)    NSString*   cityName;

@end

/**
 The PACCurrentFlightInfo class is an object that allows developers to retrieve current flight information, such as flight number, arrival gate and terminal and baggage claim.

 */
@interface PACCurrentFlightInfo : NSObject

/** The flight number for the current flight */
@property (readonly,nonatomic,copy,nonnull)     NSString*               flightNumber;

/** The carrier code for the current flight. The two-character IATA code identifies the airline on arrival and departure signs and passenger tickets. */
@property (readonly,nonatomic,copy,nullable)    NSString*               carrierCode;

/** The arrival gate for the current flight */
@property (readonly,nonatomic,copy,nullable)    NSString*               arrivalGate;

/** The baggage claim information for the current flight */
@property (readonly,nonatomic,copy,nullable)    NSString*               baggageClaim;

/** The airport terminal where the current flight is arriving */
@property (readonly,nonatomic,copy,nullable)    NSString*               arrivalTerminal;

/** The airport where the current flight is departing */
@property (readonly,nonatomic,strong,nonnull)   PACBasicAirportInfo*    departureAirport;

/** The airport where the current flight is arriving */
@property (readonly,nonatomic,strong,nonnull)   PACBasicAirportInfo*    arrivalAirport;

/** The scheduled arrival time for the current flight. The time is expressed in local time using a 24 hour format, based on ISO-8601 */
@property (readonly,nonatomic,strong,nullable)  NSDate*                 arrivalTimeScheduled;

/** The estimated arrival time for the current flight. The time is expressed in local time using a 24 hour format, based on ISO-8601 */
@property (readonly,nonatomic,strong,nullable)  NSDate*                 arrivalTimeEstimated;

/** The arrival status, such as on time or en route, for the current flight */
@property (readonly,nonatomic,copy,nullable)    NSString*               arrivalStatus;

@end

@interface PACCurrentFlightInfo (RawDatesText)

/** The raw text reported for the scheduled arrival time for the connecting flight. If the property arrivalTimeScheduled is nil, this property may be displayed as-is or further parsed. There is no guarantee that the date is in a known format. */
@property (readonly,nonatomic,copy,nullable)    NSString*               arrivalTimeScheduledRawDate;

/** The raw text reported for the estimated arrival time for the connecting flight. If the property arrivalTimeEstimated is nil, this property may be displayed as-is or further parsed. There is no guarantee that the date is in a known format. */
@property (readonly,nonatomic,copy,nullable)    NSString*               arrivalTimeEstimatedRawDate;

@end

/**
 The PACConnectingFlightInfo class is an object that describes information about a connecting flight
 */
@interface PACConnectingFlightInfo : NSObject

/** The carrier code for the connecting flight. The two-character IATA code identifies the airline on arrival and departure signs and passenger tickets. */
@property (readonly,nonatomic,copy,nullable)    NSString*               carrierCode;


/** The airline operator information for the connecting flight. */
@property (readonly,nonatomic,copy,nullable)    NSString*               flightOperator;


/** The flight number for the connecting flight */
@property (readonly,nonatomic,copy,nonnull)     NSString*               flightNumber;

/** The airport where the connecting flight is arriving */
@property (readonly,nonatomic,strong,nonnull)   PACBasicAirportInfo*    arrivalAirport;

/** The airport where the connecting flight is departing */
@property (readonly,nonatomic,copy,nullable)    NSString*               departureTerminal;

/** The departure gate for the connecting flight. The time is expressed in local time using a 24 hour format, based on ISO-8601 */
@property (readonly,nonatomic,copy,nullable)    NSString*               departureGate;

/** The scheduled departure time for the connecting flight. The time is expressed in local time using a 24 hour format, based on ISO-8601  */
@property (readonly,nonatomic,copy,nullable)    NSDate*                 departureTimeScheduled;

/** The estimated departure time for the connecting flight. The time is expressed in local time using a 24 hour format, based on ISO-8601 */
@property (readonly,nonatomic,copy,nullable)    NSDate*                 departureTimeEstimated;

/** The departure status, such as En Route or Landed,  for the connecting flight */
@property (readonly,nonatomic,copy,nullable)    NSString*               departureStatus;

@end

@interface PACConnectingFlightInfo (RawDatesText)

/** The raw text reported for the scheduled departure time for the connecting flight. If the property departureTimeScheduled is nil, this property may be displayed as-is or further parsed. There is no guarantee that the date is in a known format. */
@property (readonly,nonatomic,copy,nullable)    NSString*               departureTimeScheduledRawDate;

/** The raw text reported for the estimated departure time for the connecting flight. If the property departureTimeEstimated is nil, this property may be displayed as-is or further parsed. There is no guarantee that the date is in a known format. */
@property (readonly,nonatomic,copy,nullable)    NSString*               departureTimeEstimatedRawDate;

@end

/**
 The PACConnectingGateInfo class is an object describing the information passed in to the completionHandler of the method -connectingGateInfoWithSortOrder:sortedOnField:completed:
 */
@interface PACConnectingGateInfo : NSObject

/** The information about the current flight */
@property (readonly,nonatomic,strong,nonnull)   PACCurrentFlightInfo*               currentFlightInfo;

/** The information about connecting flights */
@property (readonly,nonatomic,copy,nonnull)     NSArray<PACConnectingFlightInfo*>*  connectingFlights;

@end

/**
 * The order that the connecting gate information should be sorted, such as by ascending or descending.
 * @owner PACConnectingGateV1
 */
typedef enum
{
    /** Data should be sorted in ascending order. */
    PACConnectingGateSortOrderAscending     = -1,
    /** Sort order of data should be in descending order. */
    PACConnectingGateSortOrderDescending    = 1,
}
PACConnectingGateSortOrder;

/**
 * The field in the returned connecting gate information to be sorted.
 * @owner PACConnectingGateV1
 */
typedef enum
{
    /** The data will be sorted in ascending order based on the scheduled departure time. The specified sort order will be ignored. */
    PACConnectingGateSortFieldDepartureTimeScheduled    = 0,
    /** The data will be sorted in the specified ascending or descending order based on flight number. */
    PACConnectingGateSortFieldFlightNumber              = 1,
    /** The data will be sorted in the specified ascending or descending order based on the arrival airport. */
    PACConnectingGateSortFieldArrivalAirport            = 2,
}
PACConnectingGateSortField;

/**
  PACConnectingGateV1 allows developers to retrieve connecting flight information,such as flight number, arrival airport and departure terminal, gate and time, that their apps can display to passengers. 

 [Download Sample Project](../../samples/PACConnectingGate/SampleConnectingGateApplication.zip)
 
  ***Example***
        
        PACConnectingGateV1*    connectingGateService;
 
        -(void)viewDidLoad
        {
            [super viewDidLoad];

            // Initialize connecting flight information service
            [PACConnectingGateV1 initServiceWithCompletionBlock:
            ^(id serviceObject, NSError *error)
            {
                if (nil != error)
                {
                    NSLog (@"Failed to initialize the service %@. Error: %@.",NSStringFromClass([serviceObject class]),error);
                }
                else
                {
                    self.connectingGateService = serviceObject;
                }
            }];
        }

        #pragma mark - Button events
        -(IBAction )onGetInfoClicked:(id )sender
        {
            if (nil != self.connectingGateService)
            {
                // Get information, such as flight number, arrival airport and departure terminal, gate number and time, for connecting flights. 
                [self.connectingGateService
                    connectingGateInfoWithSortOrder:0
                    sortedOnField:PACConnectingGateSortFieldDepartureTimeScheduled
                    completed:^(PACConnectingGateInfo* connectingGateInfo, NSError *error)
                    {
                        if (nil != error)
                        {
                            if ((PACConnectingGateErrorNoConnectingGateInfo == error.code) &&
                                [error.domain isEqualToString:PACConnectingGateErrorDomain])
                            {
                                NSLog (@"No connecting gate info available");
                            }
                            else
                            {
                                NSLog (@"Error occurred. error: %@",error);
                            }
                        }
                        else
                        {
                            NSLog (@"Connecting gate info: %@",connectingGateInfo);
                        }
                    }];
            }
        }
 
 */

@interface PACConnectingGateV1 : InFlightService

/**
 * Get connecting flight information, such as flight number, arrival airport and departure terminal, gate number and time.
 * @param sortOrder Order that the data should be sorted, such as ascending or descending.
 * @param sortedField Field, such as departure time, flight number or arrival airport, by which the data should be sorted.
 * @param completionHandler The completion block that will be called when the request was made.
 *
 * - *connectingGateInfo* The connecting flight information that will be returned if successful.
 * - *error* The NSError object that will be set if an error occurs. Please refer to PACConnectingGateErrorCode for error codes.
 *
 */
-(void)connectingGateInfoWithSortOrder:(PACConnectingGateSortOrder )sortOrder
    sortedOnField:(PACConnectingGateSortField )sortedField
    completed:(nullable void(^)(PACConnectingGateInfo* __nullable connectingGateInfo, NSError* __nullable error) )completionHandler;

@end

#pragma mark - Events
/**
 * The connecting flight information has changed.
 * @owner PACConnectingGateV1
 *
 * The notification includes no payload.  The caller should perform a call to get the latest information, if needed. (Who is the caller?)
 *
 */
FOUNDATION_EXPORT NSString* __nonnull const PACConnectingGateInfoWasUpdatedNotification;

/**
 * The connecting flight information has been cleared.
 * @owner PACConnectingGateV1
 *
 * The notification includes no payload.  The developer should release any current connecting flight information by perfoming a call to get the latest information, if needed. 
 *
 */
FOUNDATION_EXPORT NSString* __nonnull const PACConnectingGateInfoWasClearedNotification;

/**
 * Connecting Gate API error codes.
 * @owner PACConnectingGateV1
 */
typedef enum 
{
    /** The request has failed due to an unknown error. */
    PACConnectingGateErrorUnknown               = 1000,
    /** The request has missing or incorrect parameters. */
    PACConnectingGateErrorBadRequest            = 1001,
    /** The request has failed due to the service being unavailable. */
    PACConnectingGateErrorServiceNotFound       = 1002,
    /** The request has failed due to an unexpected internal error. */
    PACConnectingGateErrorInternalError         = 1003,
    /** The request has failed due to an unexpected response from the server. */
    PACConnectingGateErrorBadResponse           = 1004,
    /** The request has failed due to a network connection error. */
    PACConnectingGateErrorConnectionError       = 1005,
    /** The request has failed due to not having any connecting flight information. */
    PACConnectingGateErrorNoConnectingGateInfo  = 1006,
}
PACConnectingGateErrorCode;

/**
 * @constantgroup Domain Constants for NSError Results
 * @owner PACConnectingGateV1
 */
/** The domain that an NSError has when the code represents a value from PACConnectingGateErrorCode */
FOUNDATION_EXPORT NSString* __nonnull const PACConnectingGateErrorDomain;
