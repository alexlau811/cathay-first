window.cxInflight = {
    initialize: function(callback) {
        cordova.exec(callback, function(err) {
            callback('cxInflight initialize failed.');
        }, "InflightEntertainment", "initialize", []);
    },
    retrieveMovies: function(callback) {
        cordova.exec(callback, function(err) {
            callback('Nothing to echo.');
        }, "InflightEntertainment", "retrieveMovies", []);
    },
    retrieveSongs: function(callback) {
        cordova.exec(callback, function(err) {
            callback('Nothing to echo.');
        }, "InflightEntertainment", "retrieveSongs", []);
    },
    retrievePassenger: function(seatNumber, callback) {
        cordova.exec(callback, function(err) {
            callback('Nothing to echo.');
        }, "InflightEntertainment", "retrievePassenger", [seatNumber]);
    },
};
